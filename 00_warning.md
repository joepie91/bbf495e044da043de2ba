# Important warning

__You should almost never actually use this. The same applies to `fs.stat` (when used for checking existence).__

Checking whether a file exists before doing something with it, can lead to race conditions in your application. Race conditions are extremely hard to debug and, depending on where they occur, they can lead to __data loss or security holes__. Using the synchronous versions will __not__ fix this.

Generally, just do what you want to do, and handle the error if it doesn't work. This is much safer.

* __If you want to check whether a file exists, before reading it:__ just try to open the file, and handle the `ENOENT` error when it doesn't exist.
* __If you want to make sure a file doesn't exist, before writing to it:__ open the file using an [exclusive mode](https://nodejs.org/api/fs.html#fs_fs_open_path_flags_mode_callback), eg. `wx` or `ax`, and handle the error when the file already exists.
* __If you want to create a directory:__ just try to create it, and handle the error if it already exists.
* __If you want to remove a file or directory:__ just try to [unlink](https://nodejs.org/api/fs.html#fs_fs_unlink_path_callback) the path, and handle the error if it doesn't exist.

If you're *really, really sure* that you need to use `fs.exists` or `fs.stat`, then you can use the example code below. If you just want to know how to promisify an asynchronous callback that doesn't follow the nodeback convention, then you can look at the example below as well.